**Pong**

This pong game gives a reward each time you hit the ball. A small fun project to experiment with Java animation and sound. Use [maven](http://maven.apache.org/run-maven/index.html) to build it

*Build and run*

    mvn package
    java -jar target/pong-0.1-SNAPSHOT.jar

*Manual*

Space: Start game  
Escape: Quit  
F1-F4: Adjust paddles size  
F5-F6: Adjust ball speed  
F7: Adjust paddles and ball color
 
