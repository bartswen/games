package bs;

public class PressKeyGameAction {
	private String name;

	private boolean pressed;

	public PressKeyGameAction(String name) {
		super();
		this.name = name;
	}

	public synchronized void press() {
		pressed = true;
	}

	public synchronized boolean wasPressedAndReset() {
		boolean waspressed = pressed;
		pressed = false;
		return waspressed;
	}

	public void reset() {
		pressed = false;
	}

}
