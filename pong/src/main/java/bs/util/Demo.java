package bs.util;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.xml.transform.Source;

import sun.audio.AudioStream;
import bs.sound.Sound;
import bs.sound.SoundManager;

public class Demo {

    /**
     * @param args
     */
    public static void main(String[] args) {
         playBeat1();
        // playSynth02AsClip();
//        playSynth02AsStream();
        System.out.println("Done");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void playBeat1() {
        // uncompressed, 44100Hz, 8-bit, mono, signed, little-endian
        final AudioFormat playbackFormat = new AudioFormat(22050, 8, 1, false, false);
        
        System.out.println("Max simultanuous:" + SoundManager.getMaxSimultaneousSounds(playbackFormat));

        SoundManager soundManager = new SoundManager(playbackFormat);
        InputStream is = Demo.class.getResourceAsStream("/BEAT1.WAV");
        Sound sound = soundManager.getSound(is);

        soundManager.play(sound);
    }

    private static void playSynth02AsClip() {
        try {
            // uncompressed, 44100Hz, 8-bit, mono, signed, little-endian
            InputStream is = Demo.class.getResourceAsStream("/synth02.wav");
            AudioInputStream stream = AudioSystem.getAudioInputStream(is);
            AudioFormat format = stream.getFormat();

            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip clip = (Clip) AudioSystem.getLine(info);

            clip.open(stream);
            clip.start();
        } catch (UnsupportedAudioFileException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }

        return;
    }

    private static void playSynth02AsStream() {
        try {
            // uncompressed, 44100Hz, 8-bit, mono, signed, little-endian
            InputStream is = Demo.class.getResourceAsStream("/synth02.wav");
            AudioInputStream stream = AudioSystem.getAudioInputStream(is);
            AudioFormat format = stream.getFormat();

            byte[] bytes = getSamples(stream, format);

            ByteArrayInputStream sourceBis = new ByteArrayInputStream(bytes);

            int bufferSize = format.getFrameSize() * Math.round(format.getSampleRate() / 10);
            byte[] buffer = new byte[bufferSize];
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
            SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(format, buffer.length);
            line.start();

            int numBytesRead = 0;
            while (numBytesRead != -1) {
                numBytesRead = sourceBis.read(buffer, 0, buffer.length);
                if (numBytesRead != -1) {
                    line.write(buffer, 0, numBytesRead);
                }
            }
            line.drain();
            line.close();

        } catch (UnsupportedAudioFileException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (LineUnavailableException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return;
    }

    private static byte[] getSamples(AudioInputStream stream, AudioFormat format) {
        try {
            int length = (int) (stream.getFrameLength() * format.getFrameSize());
            byte[] samples = new byte[length];
            DataInputStream is = new DataInputStream(stream);
            is.readFully(samples);
            return samples;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
