package bs;

import java.awt.Color;
import java.awt.Graphics;

public final class FlowerDrawer {
    public static final int WIDTH = 80;
    public static final int HEIGHT = 160;

    private static final int[][] LEAFES_LOCATION = { { 30, 0 }, { 50, 9 }, { 60, 30 }, { 50, 51 },
            { 30, 60 }, { 10, 51 }, { 0, 30 }, { 10, 9 } };

    private FlowerDrawer() {
    }

    /**
     * 
     * @param g
     * @param oX
     * @param oY
     * @param leafs Number of leafs to draw, 0-8
     */
    public static void draw(Graphics g, int oX, int oY, int leafs) {
        if (leafs > 8) {
            leafs = 8;
        }
        if (leafs < 0) {
            leafs = 0;
        }
        g.setColor(Color.green);
        g.drawLine(oX + 38, oY + 60, oX + 30, oY + 160);
        g.fillOval(oX + 16, oY + 95, 20, 10);
        g.fillOval(oX + 32, oY + 120, 20, 10);
        g.setColor(Color.yellow);
        g.fillOval(oX + 20, oY + 20, 40, 40);
        g.setColor(Color.white);
        for (int i = 0; i < leafs; i++) {
            g.fillOval(oX + LEAFES_LOCATION[i][0], oY + LEAFES_LOCATION[i][1], 20, 20);
        }
    }

    /**
     * 
     * @param g
     * @param oX
     * @param oY
     * @param leafs Number of leafs to draw, 0-8
     * @param size 1 = 100%, 0.5 = 50%, 1.5 = 150%
     */
    public static void draw(Graphics g, int oX, int oY, int leafs, float size) {
        g.setColor(Color.green);
        int halfWidth = WIDTH / 2;
        int halfHeight = HEIGHT / 2;
        g.drawLine(oX + Math.round(halfWidth - halfWidth * size) + Math.round(38 * size),
                oY + Math.round(halfHeight - halfHeight * size) + Math.round(60 * size),
                oX + Math.round(halfWidth - halfWidth * size) + Math.round(30 * size),
                oY + Math.round(halfHeight - halfHeight * size) + Math.round(160 * size));
        g.fillOval(oX + Math.round(halfWidth - halfWidth * size) + Math.round(16 * size),
                oY + Math.round(halfHeight - halfHeight * size) + Math.round(95 * size),
                Math.round(20 * size), Math.round(10 * size));
        g.fillOval(oX + Math.round(halfWidth - halfWidth * size) + Math.round(32 * size),
                oY + Math.round(halfHeight - halfHeight * size) + Math.round(120 * size),
                Math.round(20 * size), Math.round(10 * size));
        g.setColor(Color.yellow);
        g.fillOval(oX + Math.round(halfWidth - halfWidth * size) + Math.round(20 * size),
                oY + Math.round(halfHeight - halfHeight * size) + Math.round(20 * size),
                Math.round(40 * size), Math.round(40 * size));
        g.setColor(Color.white);
        for (int i = 0; i < leafs; i++) {
            g.fillOval(
                    oX + Math.round(halfWidth - halfWidth * size) + Math.round(LEAFES_LOCATION[i][0] * size),
                    oY + Math.round(halfHeight - halfHeight * size)
                            + Math.round(LEAFES_LOCATION[i][1] * size), Math.round(20 * size),
                    Math.round(20 * size));
        }
    }

}
