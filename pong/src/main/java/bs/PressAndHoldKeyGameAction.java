package bs;

public class PressAndHoldKeyGameAction {
	private String name;

	private long start;

	private boolean pressed;

	private int pressDuration;

	public PressAndHoldKeyGameAction(String name) {
		super();
		this.name = name;
	}

	public synchronized void press(long pressStart) {
//		System.out.println(String.format("press %s    %d", name,
//				System.currentTimeMillis()));
		if (!pressed) {
			start = pressStart;
			pressed = true;
		}
	}

	public synchronized void release(long end) {
//		System.out.println(String.format("release %s   %d\n", name,
//				System.currentTimeMillis()));
		if (pressed) {
			pressDuration = (int) (end - start);
			pressed = false;
		}
	}

	public synchronized int getAndResetPressDuration(long now) {
		final int duration;
		if (pressed) {
			duration = (int) (now - start);
			start = now;
		} else {
			duration = pressDuration;
			pressDuration = 0;
		}
		return duration;
	}

	public void reset() {
		start = pressDuration = 0;
		pressed = false;
	}

	public boolean isPressed() {
		return pressed;
	}

}
